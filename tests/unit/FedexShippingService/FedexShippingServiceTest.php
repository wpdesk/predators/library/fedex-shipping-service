<?php

use FedEx\RateService\ComplexType\RateRequest;
use Psr\Log\NullLogger;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Shop\ShopSettings;
use WPDesk\FedexShippingService\FedexApi\Soap\FedexSoapRateRequestBuilder;
use WPDesk\FedexShippingService\FedexApi\Soap\FedexSender;
use WPDesk\FedexShippingService\FedexApi\Soap\Sender;
use WPDesk\FedexShippingService\FedexShippingService;
use WPDesk\FedexShippingService\FedexValidateShipment;

class FedexShippingServiceTest extends \PHPUnit\Framework\TestCase {

	/**
	 * @return FedexShippingService
	 */
	private function get_fedex_service() {
		return new FedexShippingService( new NullLogger(), $this->createMock( ShopSettings::class ) );
	}

	public function test_get_settings_definition_instance() {
		$fedex = $this->get_fedex_service();
		$this->assertInstanceOf( \WPDesk\AbstractShipping\Settings\SettingsDefinition::class, $fedex->get_settings_definition() );
	}



	public function test_get_unique_id() {
		$fedex = $this->get_fedex_service();
		$this->assertNotEmpty( $fedex->get_unique_id() );
		$this->assertInternalType( 'string', $fedex->get_unique_id() );
	}

	public function test_get_name() {
		$fedex = $this->get_fedex_service();
		$this->assertNotEmpty( $fedex->get_name() );
		$this->assertInternalType( 'string', $fedex->get_name() );
	}

	public function test_get_description() {
		$fedex = $this->get_fedex_service();
		$this->assertNotEmpty( $fedex->get_description() );
		$this->assertInternalType( 'string', $fedex->get_description() );
	}

	public function test_weight_not_exceeded() {
		$shipment = ShipmentFaker::build_blank_shipment();
		$validate_shipment = new FedexValidateShipment( $shipment, new NullLogger() );
		$this->assertEquals( false, $validate_shipment->is_weight_exceeded(), 'Max weight exceeded.' );
	}

	public function test_sender() {
		$fedex = $this->get_fedex_service();
		$request = $this->createMock( RateRequest::class );
		$fedex->set_sender( new FedexSender( new NullLogger(), true, true ) );
		$this->assertInstanceOf( Sender::class, $fedex->get_sender( $request ) );
	}

	public function test_request_builder() {
		$shipment = ShipmentFaker::build_blank_shipment();
		$settings = $this->createMock( SettingsValues::class );
		$shop_settings = $this->createMock( ShopSettings::class );
		$request_builder = new FedexSoapRateRequestBuilder( $settings, $shipment, $shop_settings );
		$this->assertInstanceOf( RateRequest::class, $request_builder->build_request());
	}
}
