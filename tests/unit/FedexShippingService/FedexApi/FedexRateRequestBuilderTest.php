<?php

use FedEx\RateService\ComplexType\RateRequest;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Shop\ShopSettings;
use WPDesk\FedexShippingService\FedexApi\Soap\FedexSoapRateRequestBuilder;

class FedexRateRequestBuilderTest extends \PHPUnit\Framework\TestCase {
	protected function setUp() {
		parent::setUp();
		$this->markTestSkipped("ShopSettings should be moved to abstract.");
	}

	public function test_can_build_fedex_request() {
		$fedexSettingsMock = $this->createMock( SettingsValues::class );
		$shopSettingsMock  = $this->createMock( ShopSettings::class );

		$shipment = ShipmentFaker::build_blank_shipment();

		$requestBuilder = new FedexSoapRateRequestBuilder( $fedexSettingsMock, $shipment, $shopSettingsMock );
		$this->assertInstanceOf( RateRequest::class, $requestBuilder->build_request() );
	}

	/**
	 * Is residential return false.
	 */
	public function test_is_residential_return_false() {
		$fedexSettingsMock = $this->createMock( SettingsValues::class );
		$shopSettingsMock  = $this->createMock( ShopSettings::class );
		$shipment = ShipmentFaker::build_blank_shipment();
		$requestBuilder = new FedexSoapRateRequestBuilder( $fedexSettingsMock, $shipment, $shopSettingsMock );
		$request = $requestBuilder->build_request();
		$this->assertFalse( $request->RequestedShipment->Recipient->Address->Residential );
	}
}
