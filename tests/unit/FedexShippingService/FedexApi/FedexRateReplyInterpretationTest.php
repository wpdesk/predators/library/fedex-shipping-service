<?php


use FedEx\RateService\ComplexType\Notification;
use FedEx\RateService\ComplexType\RatedShipmentDetail;
use FedEx\RateService\ComplexType\RateReply;
use FedEx\RateService\ComplexType\RateReplyDetail;
use FedEx\RateService\SimpleType\RateTypeBasisType;
use WPDesk\FedexShippingService\FedexApi\Soap\FedexSoapRateReplyInterpretation;

class FedexRateReplyInterpretationTest extends \PHPUnit\Framework\TestCase {
	const RATE_SERVICE_TYPE = 'SOME TYPE';
	const RATE_SERVICE_NAME = 'SOME NAME';
	const RATE_SERVICE_CURRENCY = 'PLN';
	const RATE_SERVICE_MONEY_AMOUNT = 101.01;
	const NOTIFICATION_MESSAGE = 'SOME MESSAGE';

	/**
	 * test if get_rates can convert from FedexRateReply to SingleRate
	 */
	public function test_get_rates() {
		$artificialReplyDetail                                  = new RateReplyDetail();
		$artificialReplyDetail->ServiceType                     = self::RATE_SERVICE_TYPE;
		$artificialReplyDetail->ServiceDescription->Description = self::RATE_SERVICE_NAME;

		$artificialDetail                                               = new RatedShipmentDetail();
		$artificialDetail->ShipmentRateDetail->TotalNetCharge->Currency = self::RATE_SERVICE_CURRENCY;
		$artificialDetail->ShipmentRateDetail->TotalNetCharge->Amount   = self::RATE_SERVICE_MONEY_AMOUNT;
		$artificialDetail->ShipmentRateDetail->RateType                 = \FedEx\RateService\SimpleType\ReturnedRateType::_PAYOR_LIST_PACKAGE;

		$artificialReply                             = new RateReply();
		$artificialReplyDetail->RatedShipmentDetails = [ $artificialDetail ];
		$artificialReply->RateReplyDetails           = [ $artificialReplyDetail ];

		$replyInterpreter = new FedexSoapRateReplyInterpretation( $artificialReply, false, RateTypeBasisType::_LIST );

		$rates     = $replyInterpreter->get_ratings();
		$firstRate = reset( $rates );

		$this->assertEquals( $firstRate->total_charge->amount, self::RATE_SERVICE_MONEY_AMOUNT, 'Incorrect money amount from rate' );
		$this->assertEquals( $firstRate->total_charge->currency, self::RATE_SERVICE_CURRENCY, 'Incorrect money currency from rate' );
		$this->assertEquals( $firstRate->service_type, self::RATE_SERVICE_TYPE, 'Incorrect service type from rate' );
		$this->assertEquals( $firstRate->service_name, self::RATE_SERVICE_NAME, 'Incorrect service name from rate' );
	}

	/**
	 * test if can detect error in reply
	 */
	public function test_reply_error() {
		$artificialReply                  = new RateReply();
		$artificialReply->HighestSeverity = 'ERROR';

		$replyInterpreter = new FedexSoapRateReplyInterpretation( $artificialReply, true, RateTypeBasisType::_LIST );
		$this->assertTrue( $replyInterpreter::has_reply_error( $artificialReply ) );
	}


	/**
	 * test if can detect warning in reply
	 */
	public function test_reply_warning() {
		$artificialReply                  = new RateReply();
		$artificialReply->HighestSeverity = 'WARNING';

		$replyInterpreter = new FedexSoapRateReplyInterpretation( $artificialReply, true, RateTypeBasisType::_LIST );
		$this->assertTrue( $replyInterpreter::has_reply_warning( $artificialReply ) );

	}

	/**
	 * test if can get notification messages from reply
	 */
	public function test_get_reply_message() {
		$artificialReply                = new RateReply();
		$notification                   = new Notification();
		$notification->Message          = self::NOTIFICATION_MESSAGE;
		$artificialReply->Notifications = [ $notification ];

		$replyInterpreter = new FedexSoapRateReplyInterpretation( $artificialReply, true, RateTypeBasisType::_LIST );
		$this->assertEquals( self::NOTIFICATION_MESSAGE, $replyInterpreter::get_reply_message( $artificialReply ) );
	}
}
