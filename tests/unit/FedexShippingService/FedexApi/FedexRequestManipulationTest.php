<?php


use WPDesk\AbstractShipping\Shipment\Weight;
use WPDesk\FedexShippingService\FedexApi\FedexRequestManipulation;

class FedexRequestManipulationTest extends \PHPUnit\Framework\TestCase {
	/**
	 * Provides test fixtures for test_iso_to_fedex_province
	 *
	 * @return array [ [ iso province, fedex province ] ]
	 */
	public function woocommerce_to_fedex_province_provider() {
		return [
			[ 'A', '' ], // Spanish - Alicante
			[ 'ACT', '' ], // Australia - Australian Capital Territory
			[ 'CN1', '' ], // Chinese - Yunnan
			[ 'CN32', '' ], // Chinese - Xinjiang
			[ 'PL-DS', '' ], // Poland - our standard
			[ 'BD-05', '' ], // Bangladeshi - Bagerhat
			[ 'BG-21', '' ], // Bulgarian - Smolyan
			[ 'PY-ASU', '' ], // Paraguay - Asunci&oacute;n
			[ 'AB', 'AB' ], // Canada
			[ 'AP', 'AP' ], // USA
			[ 'WA', 'WA' ], // Australia - Western and also USA - Washington
		];
	}

	/**
	 * Provides test fixtures for test_can_conver_weight
	 *
	 * @return array  [ [ weight unit, fedex weight unit ] ]
	 */
	public function abstract_shipping_to_fedex_weight_unit_provider() {
		return [
			[ Weight::WEIGHT_UNIT_KG, 'KG' ],
			[ Weight::WEIGHT_UNIT_LB, 'LB' ],
		];
	}

	/**
	 * @dataProvider woocommerce_to_fedex_province_provider
	 *
	 * @param $iso_province
	 * @param $fedex_province
	 */
	public function test_filter_province_for_fedex( $iso_province, $fedex_province ) {
		$this->assertEquals( $fedex_province, FedexRequestManipulation::filter_province_for_fedex( $iso_province ), "Fedex needs 2 letter province code for Canada and USA and '' for others" );
	}

	public function test_can_convert_utf7() {
		$some_utf8_string = 'ęóźąś∂łĘŁĄŽ';
		$this->assertEquals( mb_convert_encoding( $some_utf8_string, 'UTF-7', 'UTF-8' ), FedexRequestManipulation::convert_to_utf7( $some_utf8_string ), "Invalid utf7 conversion" );
	}

	/**
	 * @dataProvider abstract_shipping_to_fedex_weight_unit_provider
	 *
	 * @param $woocommerce_weight
	 * @param $fedex_weight
	 */
	public function test_can_convert_weight( $woocommerce_weight, $fedex_weight ) {
		$this->assertEquals( $fedex_weight, FedexRequestManipulation::convert_weight_unit( $woocommerce_weight ), "Invalid weight conversion" );
	}

	/**
	 * Can convert currency from WC ISO, to FedEx whatever standard.
	 */
	public function test_convert_currency() {
		$pound        = 'GBP';
		$poundInFedex = 'UKL';
		$this->assertEquals( $poundInFedex, FedexRequestManipulation::convert_currency_to_fedex( $pound ) );
	}
}
