<?php

use WPDesk\AbstractShipping\Settings\SettingsValuesAsArray;
use WPDesk\AbstractShipping\Shop\ShopSettings;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

class FedexSettingsDefinitionTest extends \PHPUnit\Framework\TestCase {

	/**
	 * option name for service FEDEX_EXPRESS_SAVER in CA should be FedEx Economy
	 */
	public function test_get_form_fields_works_for_california() {
		$shopSettingsMock = $this->createMock( ShopSettings::class );
		$shopSettingsMock->expects( $this->once() )->method( 'get_origin_country' )->willReturn( 'CA' );

		/** @var ShopSettings $shopSettingsMock */
		$definition           = new FedexSettingsDefinition( $shopSettingsMock );
		$fields               = $definition->get_form_fields();
		$services_field       = array_filter( $fields, function ( $value, $key ) {
			return $key === 'services';
		}, ARRAY_FILTER_USE_BOTH );
		$express_saver_option = array_filter( $services_field['services']['options'], function ( $value, $key ) {
			return $key === 'FEDEX_EXPRESS_SAVER';
		}, ARRAY_FILTER_USE_BOTH );
		$this->assertEquals( 'FedEx Economy', reset( $express_saver_option ), 'For california FEDEX_EXPRESS_SAVER should be named FedEx Economy' );
	}

	/**
	 * form fields are as array and have basic properties
	 */
	public function test_get_form_fields_are_valid_fields() {
		/** @var ShopSettings $shopSettingsMock */
		$shopSettingsMock = $this->createMock( ShopSettings::class );
		$definition       = new FedexSettingsDefinition( $shopSettingsMock );
		$fields           = $definition->get_form_fields();
		$this->assertInternalType( 'array', $fields, 'Fields definition must be array' );
		foreach ( $fields as $field ) {
			$this->assertInternalType( 'string', $field['title'], 'Every field must have title:string field' );
			$this->assertInternalType( 'string', $field['type'], 'Every field must have type:string field' );
		}
	}

	/**
	 * form fields has destination_address_type key
	 */
	public function test_form_fields_has_destination_address_type() {
		/** @var ShopSettings $shopSettingsMock */
		$shopSettingsMock = $this->createMock( ShopSettings::class );
		$definition       = new FedexSettingsDefinition( $shopSettingsMock );
		$fields           = $definition->get_form_fields();
		$this->assertArrayHasKey( 'destination_address_type', $fields, 'Can\'t find destination_address_type' );
	}


	/**
	 * validate_settings is always true at the moment
	 */
	public function test_validate_settings_always_valid() {
		/** @var ShopSettings $shopSettingsMock */
		$shopSettingsMock = $this->createMock( ShopSettings::class );
		$definition       = new FedexSettingsDefinition( $shopSettingsMock );
		$is_valid         = $definition->validate_settings( new SettingsValuesAsArray( [] ) );

		$this->assertTrue( $is_valid, 'Any settings should be valid for now' );
	}
}
