<?php

use Psr\Log\NullLogger;
use WPDesk\AbstractShipping\Shipment\Weight;
use WPDesk\FedexShippingService\FedexApi\FedexRequestManipulation;
use WPDesk\FedexShippingService\FedexShippingService;
use WPDesk\FedexShippingService\FedexValidateShipment;
use WPDesk\WooCommerceShipping\ShopSettings;

class FedexValidateShipmentTest extends \PHPUnit\Framework\TestCase {
	/**
	 * Provides test max weights for defined units.
	 *
	 * @return array  [ [ weight unit, max weight ] ]
	 */
	public function abstract_shipping_to_fedex_weight_unit_provider() {
		return [
			[ Weight::WEIGHT_UNIT_G, FedexValidateShipment::FEDEX_MAX_WEIGHTS[ WEIGHT::WEIGHT_UNIT_G ] ],
			[ Weight::WEIGHT_UNIT_KG, FedexValidateShipment::FEDEX_MAX_WEIGHTS[ WEIGHT::WEIGHT_UNIT_KG ] ],
			[ Weight::WEIGHT_UNIT_LB, FedexValidateShipment::FEDEX_MAX_WEIGHTS[ WEIGHT::WEIGHT_UNIT_LB ] ],
			[ Weight::WEIGHT_UNIT_OZ, FedexValidateShipment::FEDEX_MAX_WEIGHTS[ WEIGHT::WEIGHT_UNIT_OZ ] ],

		];
	}

	/**
	 * @dataProvider abstract_shipping_to_fedex_weight_unit_provider
	 *
	 * @param $unit
	 * @param $max_weight
	 */
	public function test_is_weight_exceeded( $unit, $max_weight ) {
		$shipment = ShipmentFaker::build_blank_shipment();
		$shipment->packages[0]->items[0]->weight->weight = $max_weight + 10;
		$shipment->packages[0]->items[0]->weight->weight_unit = $unit;
		$validate = new FedexValidateShipment( $shipment, new NullLogger() );
		$this->assertTrue( $validate->is_weight_exceeded() );
	}

}
