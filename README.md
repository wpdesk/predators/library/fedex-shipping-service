[![pipeline status](https://gitlab.com/wpdesk/fedex-shipping-service/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/fedex-shipping-service/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/fedex-shipping-service/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/fedex-shipping-service/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/fedex-shipping-service/v/stable)](https://packagist.org/packages/wpdesk/fedex-shipping-service) 
[![Total Downloads](https://poser.pugx.org/wpdesk/fedex-shipping-service/downloads)](https://packagist.org/packages/wpdesk/fedex-shipping-service) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/fedex-shipping-service/v/unstable)](https://packagist.org/packages/wpdesk/fedex-shipping-service) 
[![License](https://poser.pugx.org/wpdesk/fedex-shipping-service/license)](https://packagist.org/packages/wpdesk/fedex-shipping-service) 

Fedex Shipping Service
======================

