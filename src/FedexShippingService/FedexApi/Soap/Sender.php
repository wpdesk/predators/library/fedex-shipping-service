<?php

namespace WPDesk\FedexShippingService\FedexApi\Soap;

use FedEx\RateService\ComplexType\RateReply;
use FedEx\RateService\ComplexType\RateRequest;

/**
 * Sender class interface.
 *
 * @package WPDesk\FedexShippingService\FedexApi
 */
interface Sender {

	/**
	 * Send request.
	 *
	 * @param RateRequest $request Request.
	 *
	 * @return RateReply
	 */
	public function send( RateRequest $request );

}
