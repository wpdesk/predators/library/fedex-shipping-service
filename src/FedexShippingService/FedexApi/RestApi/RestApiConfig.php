<?php

namespace WPDesk\FedexShippingService\FedexApi\RestApi;

use CageA80\FedEx\Support\EnvironmentType;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

class RestApiConfig {

	/**
	 * @var SettingsValues
	 */
	private $settings;

	/**
	 * @var string
	 */
	private $auth_provider_class;

	public function __construct( SettingsValues $settings, string $auth_provider_class = null ) {
		$this->settings            = $settings;
		$this->auth_provider_class = $auth_provider_class;
	}

	public function prepare_config() {
		$config = [
			'accountNumber' => $this->settings->get_value( FedexSettingsDefinition::FIELD_ACCOUNT_NUMBER ),
			'key'           => $this->settings->get_value( FedexSettingsDefinition::FIELD_REST_API_KEY ),
			'secret'        => $this->settings->get_value( FedexSettingsDefinition::FIELD_REST_SECRET_KEY ),
			'environment'   => EnvironmentType::PRODUCTION,
		];
		if ( $this->auth_provider_class ) {
			$config['authProvider'] = $this->auth_provider_class;
		}

		return $config;
	}

}
