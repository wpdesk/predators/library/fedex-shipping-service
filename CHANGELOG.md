## [2.12.4] - 2024-06-18
### Fixed
- Province codes in REST API

## [2.12.3] - 2024-06-04
### Fixed
- REST API preferred currency

## [2.12.2] - 2024-05-27
### Fixed
- Item value rounding precision

## [2.12.1] - 2024-05-27
### Fixed
- REST API minimal package weight
- REST API test mode disabled

## [2.12.0] - 2024-05-24
### Added
- Restoring the REST API

## [2.11.0] - 2024-04-15
### Added
- services: FedEx First, FedEx Priority Express, FedEx Economy

## [2.10.0] - 2024-03-14
### Changed
- Temporary disable of FedEx REST API

## [2.9.1] - 2024-02-06
### Fixed
- Deprecation notices on PHP 8.2

## [2.9.0] - 2024-02-06
### Added
- FedEx REST API

## [2.8.1] - 2023-11-23
### Fixed
- Weight calculation for single package

## [2.8.0] - 2023-11-23
### Added
- FedEx Priority service

## [2.7.0] - 2023-02-13
### Added
- services: Smart Post, Regional Economy, International Connect Plus

## [2.6.0] - 2022-11-25
### Changed
- forked Fedex API library
### Added
- get_reply as public method

## [2.5.0] - 2022-08-30
### Added
- de_DE translations

## [2.4.1] - 2022-04-19
### Fixed
- Url to Octolize

## [2.4.0] - 2022-04-19
### Changed
- Url to Octolize

## [2.3.5] - 2022-01-12
### Fixed
- Service description
- Service name

## [2.3.3] - 2022-04-08
### Removed
- Shipping zones support
### Added
- PRO version upselling 

## [2.3.1] - 2022-04-08
### Added
- Shipping Zones texts

## [2.3.0] - 2022-04-05
### Added
- Shipping Zones support

## [2.2.1] - 2022-01-11
### Fixed
- UK domestic services label

## [2.2.0] - 2022-01-10
### Added
- UK domestic services

## [2.1.0] - 2022-01-05
### Added
- FEDEX_INTERNATIONAL_PRIORITY service
### Fixed
- list rates

## [2.0.1] - 2021-08-30
### Changed
- Upgrading Version API

## [2.0.0] - 2021-08-30
### Changed
- Upgrading library PHP Fedex API Wrapper

## [1.16.3] - 2021-01-08
### Changed
- packaging type for request

## [1.16.1] - 2020-08-27
### Changed
- purpose of shipment is always send with SOLD value

## [1.16.0] - 2020-08-21
### Added
- purpose of shipment in settings and request

## [1.15.2] - 2020-06-25
### Fixed
- package weight limits

## [1.15.1] - 2020-03-04
### Changed
- debug messages

## [1.15.0] - 2020-03-02
### Added
- create_reply_interpretation method in FedexShippingService
- get_single_rate in FedexRateReplyInterpretation

## [1.14.5] - 2020-02-10
### Fixed
- translations
- supported currency links

## [1.14.4] - 2020-02-07
### Fixed
- translations

## [1.14.3] - 2020-02-07
### Fixed
- translations

## [1.14.2] - 2020-02-07
### Fixed
- connection checking - API URL (production/test)

## [1.14.1] - 2020-02-06
### Fixed
- connection checking

## [1.14.0] - 2020-02-05
### Fixed
- added ability to extend API request in PRO plugin

## [1.13.0] - 2020-02-05
### Fixed
- multicurrency support for Fedex PRO  

## [1.12.0] - 2020-02-03
### Changed
- settings fields order
### Added
- method title settings field
- API status settings field

## [1.11.1] - 2020-01-28
### Fixed
- weight units for Pack into one box by weight should be converted

## [1.11.0] - 2020-01-28
### Changed
- do not convert package units

## [1.10.0] - 2020-01-28
### Changed
- ceil on package dimensions

## [1.9.7] - 2020-01-23
### Fixed
- docs UTM

## [1.9.6] - 2020-01-22
### Fixed
- translations

## [1.9.5] - 2020-01-22
### Fixed
- translations

## [1.9.4] - 2020-01-22
### Fixed
- translations

## [1.9.3] - 2020-01-22
### Fixed
- translations

## [1.9.2] - 2020-01-20
### Fixed
- removed package weight calculation

## [1.9.1] - 2020-01-17
### Fixed
- pack to single package by default
- tests

## [1.9.0] - 2020-01-17
### Changed
- debug notices to XML

## [1.8.0] - 2020-01-15
### Added
- UNIT settings:
- Dimension unit conversion
### Changed
- Weight unit conversion


## [1.7.1] - 2019-12-06
### Added
- Packaging support
### Changed
- Rate filter refactored to FedexRateCustomServicesFilter class

## [1.6] - 2019-12-06
### Changed
- Request type -> rate type
### Added
- Fallback in a new form
- Can pack multiple items into package

## [1.5] - 2019-12-06
### Changed
- Rates changed according to https://wpdesk.myjetbrains.com/youtrack/issue/PRD-530
  for ALL we send LIST to api, and show all
  for LIST we send LIST to api and show only PAYOR_LIST
  for ACCOUNT we send NONE to api and show all

## [1.4.0] - 2019-12-05
### Removed
- Fallback

## [1.3.3] - 2019-12-04
### Fixed
- Fedex -> FedEx

## [1.3.2] - 2019-12-03
### Fixed
- Default rate type is all - fix in another place
- Fix typo in pl translation
### Changes
- New description for rate type settings

## [1.3.1] - 2019-12-02
### Fixed
- Default rate type is all

## [1.3.0] - 2019-12-02
### Added
- Fallback option in case if no rates returned
### Changed
- Removed comments in FedexSettingsDefinition
- New value in FIELD_REQUEST_TYPE definition
### Fixed
- Currency conversion
- Typo fix in PL translation


## [1.2.0] - 2019-11-27
### Fixed
- Fixed a typo in International Priority
- New service definition available in checkout when custom services are not enabled
### Added
- Insurance field
- Rate type field
### Changed
- FedexRateReply class removed as was unused
- FedexRateReplyInterpretation::has_reply_error changed to static
- FedexRateReplyInterpretation::get_reply_message changed to static
- FedexRateReplyInterpretation::has_reply_warning changed to static
- FedexSender::__construct no longer needs info about tax
- FedexRateReplyInterpretation::__construct requires rate_type

## [1.1.0] - 2019-11-19
### Added
- more services
### Fixed
- better exception catch in php 5.6


## [1.0.1] - 2019-11-14
### Modified
- libraries version

## [1.0.0] - 2019-11-13
### Added
- initial version
